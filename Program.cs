using scaffolding_net_7.src.controllers;
using scaffolding_net_7.src.data;
using scaffolding_net_7.src.interfaces;
using scaffolding_net_7.src.interfaces.IControllers;
using scaffolding_net_7.src.interfaces.IRepositories;
using scaffolding_net_7.src.interfaces.IServices;
using scaffolding_net_7.src.repository;
using scaffolding_net_7.src.services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<MongoDbContext>();

//Controllers
builder.Services.AddScoped<IHealthController, HealthController>();
builder.Services.AddScoped<IProductController, ProductController>();

//Services
builder.Services.AddScoped<IHealthService, HealthService>();
builder.Services.AddScoped<IProductService, ProductService>();

//Repositories
builder.Services.AddScoped<IProductRepository, ProductRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
