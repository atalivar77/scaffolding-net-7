using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace scaffolding_net_7.src.dto
{
    public class ResponseApi
    { 
        public int Status { get; set; }
        public required string Message { get; set; }
    }
    
    public class ResponseApi<T>
    {
        public int Status { get; set; }
        public string? Message { get; set; }
        public T? Data { get; set; }
    }
    
    public class ResponseListApi<T>
    {
        public int Status { get; set; }
        public string? Message { get; set; }
        public List<T>? Data { get; set; }
    }
}