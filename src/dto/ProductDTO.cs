using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace scaffolding_net_7.src.dto
{
   public class ProductCreateDTO
    {
        public required string Name { get; set; }

        public decimal Price { get; set; }
    }

    public class ProductUpdateDTO
    {
        public string? Name { get; set; }

        public decimal? Price { get; set; }
    }
}