using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace scaffolding_net_7.src.dto
{
    public class HealthResponse
    { 
        public int Status { get; set; }
        public required string Message { get; set; }
    }
}