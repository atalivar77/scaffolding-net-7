using MongoDB.Driver;
using scaffolding_net_7.src.models;

namespace scaffolding_net_7.src.data
{
    public class MongoDbContext
    {
        private readonly IMongoDatabase _database;

        public MongoDbContext(IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("MongoDB");
            var databaseName = configuration.GetConnectionString("MongoDBDatabaseName");

            var client = new MongoClient(connectionString);
            _database = client.GetDatabase(databaseName);
        }

        public IMongoCollection<ProductModel> Products => _database.GetCollection<ProductModel>("Products");
    }
}