using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace scaffolding_net_7.src.models
{
    public class ProductModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public required string Uuid { get; set; }
        public required string Name { get; set; }
        public decimal Price { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime CreatedAt { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime UpdatedAt { get; set; }

        public bool Active { get; set; }

        public ProductModel()
        {
            Active = true;
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }
    }
}