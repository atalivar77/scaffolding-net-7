using Microsoft.AspNetCore.Mvc;
using scaffolding_net_7.src.dto;
using scaffolding_net_7.src.models;

namespace scaffolding_net_7.src.interfaces.IControllers
{
    public interface IProductController
    {
        public  Task<ResponseApi<ProductModel>> CreateProduct([FromBody] ProductCreateDTO product);
      
        public  Task<ResponseListApi<ProductModel>> GerAllProduct();
        
        public  Task<ResponseApi<ProductModel>> GetProduct([FromRoute] string uuid);
        
        public  Task<ResponseApi> UpdateProduct([FromRoute] string uuid, [FromBody] ProductUpdateDTO productUpdateDTO);
        
        public  Task<ResponseApi> DeleteProduct([FromRoute] string uuid);
        
    }
}