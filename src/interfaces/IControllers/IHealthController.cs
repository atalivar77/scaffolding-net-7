using scaffolding_net_7.src.dto;

namespace scaffolding_net_7.src.interfaces.IControllers
{
    public interface IHealthController
    {
        public HealthResponse Get();
    }
}