
using scaffolding_net_7.src.dto;

namespace scaffolding_net_7.src.interfaces.IServices
{
    public interface IHealthService
    {
        HealthResponse GetHealth();
    }
}