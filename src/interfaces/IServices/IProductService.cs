using scaffolding_net_7.src.dto;
using scaffolding_net_7.src.models;

namespace scaffolding_net_7.src.interfaces
{
    public interface IProductService
    {
        Task<ProductModel> CreateProductAsync(ProductCreateDTO productDTO);
        Task<List<ProductModel>> GetAllProductsAsync();
        Task<ProductModel> GetProductById(string uuid);
        Task<bool> UpdateProductAsync(string uuid, ProductUpdateDTO productUpdateDTO);
        Task<bool> DeleteProductAsync(string uuid);
    }
}
