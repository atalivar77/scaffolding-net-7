

using scaffolding_net_7.src.dto;
using scaffolding_net_7.src.models;

namespace scaffolding_net_7.src.interfaces.IRepositories
{
    public interface IProductRepository
    {
        public Task<ProductModel> CreateProductAsync(ProductCreateDTO productDTO);
        public Task<List<ProductModel>> GetAllProductsAsync();

        public Task<ProductModel> GetProductByIdAsync(string uuid);

        public Task<bool> UpdateProductAsync(string uuid, ProductUpdateDTO productUpdateDTO);

        public Task<bool> DeleteProductAsync(string uuid);     
    }
}