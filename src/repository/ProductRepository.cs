using MongoDB.Driver;
using scaffolding_net_7.src.data;
using scaffolding_net_7.src.dto;
using scaffolding_net_7.src.interfaces.IRepositories;
using scaffolding_net_7.src.models;



namespace scaffolding_net_7.src.repository
{
    public class ProductRepository: IProductRepository
    {
        private readonly MongoDbContext _dbContext;

        public ProductRepository(MongoDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ProductModel> CreateProductAsync(ProductCreateDTO productDTO)
        {
            ProductModel product = new()
            {
                Uuid = Guid.NewGuid().ToString(),
                Name = productDTO.Name,
                Price = productDTO.Price

            };
            await _dbContext.Products.InsertOneAsync(product);
            return product;
        }

        public async Task<List<ProductModel>> GetAllProductsAsync()
        {
            return await _dbContext.Products.Find(x => x.Active == true).ToListAsync();
        }

        public async Task<ProductModel> GetProductByIdAsync(string uuid)
        {
            return await _dbContext.Products.Find(x => x.Uuid == uuid && x.Active == true).FirstOrDefaultAsync();
        }

        public async Task<bool> UpdateProductAsync(string uuid, ProductUpdateDTO productUpdateDTO)
        {
            ProductModel product = await GetProductByIdAsync(uuid);
            if (product != null && productUpdateDTO != null)
            {
                if (!string.IsNullOrEmpty(productUpdateDTO.Name)) product.Name = productUpdateDTO.Name;
                if (productUpdateDTO.Price.HasValue) product.Price = (decimal)productUpdateDTO.Price;
                product.UpdatedAt = DateTime.UtcNow;
                ReplaceOneResult updateResult = await _dbContext.Products.ReplaceOneAsync(x => x.Uuid == uuid, product);
                return updateResult.ModifiedCount > 0;
            }
            return false;
        }

        public async Task<bool> DeleteProductAsync(string uuid)
        {
            ProductModel product = await GetProductByIdAsync(uuid);
            if (product != null)
            {
                product.Active = false;
                ReplaceOneResult updateResult = await _dbContext.Products.ReplaceOneAsync(x => x.Uuid == uuid, product);
                return updateResult.ModifiedCount > 0;
            }
            return false;
        }
    }
}