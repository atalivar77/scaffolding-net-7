using FluentValidation;
using scaffolding_net_7.src.dto;

namespace scaffolding_net_7.src.validators
{
    public class ProductCreateDTOValidator : AbstractValidator<ProductCreateDTO>
    {
        public ProductCreateDTOValidator()
        {
            RuleFor(dto => dto.Name)
                .NotEmpty().WithMessage("Name is required.")
                .Length(100).WithMessage("Name cannot be more than 100 characters.");

            RuleFor(dto => dto.Price)
                .NotEmpty().WithMessage("Price is required.")
                .Must(BePositiveNumber).WithMessage("Price must be a positive number.");
        }

        private static bool BePositiveNumber(decimal amount)
        {
            return amount > 0;
        }
    }

    public class ProductUpdateDTOValidator : AbstractValidator<ProductUpdateDTO>
    {
        public ProductUpdateDTOValidator()
        {
            RuleFor(dto => dto.Name)
                .MaximumLength(100)
                .WithMessage("Name cannot be more than 100 characters.");

            RuleFor(dto => dto.Price)
                .Empty().When(dto => dto.Price == null || string.IsNullOrWhiteSpace(dto.Price.ToString()))
                .Must(BePositiveNumber).When(dto => !string.IsNullOrWhiteSpace(dto.Price.ToString()))
                .WithMessage("'Price' must be empty or a positive number.");
        }

        private bool BePositiveNumber(decimal? amount)
        {
            return amount > 0;
        }
    }
}