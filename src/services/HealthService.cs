using scaffolding_net_7.src.dto;
using scaffolding_net_7.src.interfaces.IServices;

namespace scaffolding_net_7.src.services
{
    public class HealthService: IHealthService
    {
        public HealthResponse GetHealth()
        {
            HealthResponse healthStatus = new() { Status = 200, Message = "Server is running correctly" };
            return healthStatus;
        }
    }
}