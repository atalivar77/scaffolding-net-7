

using scaffolding_net_7.src.dto;
using scaffolding_net_7.src.interfaces;
using scaffolding_net_7.src.interfaces.IRepositories;
using scaffolding_net_7.src.models;

namespace scaffolding_net_7.src.services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _iProductRepository;

        public ProductService(IProductRepository iProductRepository)
        {
            _iProductRepository = iProductRepository;
        }

        public async Task<ProductModel> CreateProductAsync(ProductCreateDTO productDTO)
        {
            return await _iProductRepository.CreateProductAsync(productDTO);
        }

        public async Task<List<ProductModel>> GetAllProductsAsync()
        {
            List<ProductModel> products = await _iProductRepository.GetAllProductsAsync();
            return products;
        }

        public async Task<ProductModel> GetProductById(string uuid)
        {
            ProductModel product = await _iProductRepository.GetProductByIdAsync(uuid);
            return product;
        }

        public async Task<bool> UpdateProductAsync(string uuid, ProductUpdateDTO productUpdateDTO)
        {
            return await _iProductRepository.UpdateProductAsync(uuid,productUpdateDTO);
        }

        public async Task<bool> DeleteProductAsync(string uuid)
        {
            return await _iProductRepository.DeleteProductAsync(uuid);
        }
    }
}