using Microsoft.AspNetCore.Mvc;
using scaffolding_net_7.src.dto;
using scaffolding_net_7.src.interfaces;
using scaffolding_net_7.src.interfaces.IControllers;
using scaffolding_net_7.src.models;
using scaffolding_net_7.src.validators;

namespace scaffolding_net_7.src.controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController: ControllerBase, IProductController
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseApi<ProductModel>), 200)]
        [ProducesResponseType(typeof(ResponseApi<ProductModel>), 500)]
        public async Task<ResponseApi<ProductModel>> CreateProduct([FromBody] ProductCreateDTO requestDto){
            
            ProductCreateDTOValidator validator = new ();

            // Execute the validator
            var results = validator.Validate(requestDto);

            if (!results.IsValid)
            {
                string errorValidation = string.Join(Environment.NewLine, results.Errors.Select(e => e.ErrorMessage));
                return new  ResponseApi<ProductModel>() { Status = 400 , Message = errorValidation};
            }
            
            try
            {
                ProductModel createdProduct = await _productService.CreateProductAsync(requestDto);
                return new ResponseApi<ProductModel>() { Status = 200, Data = createdProduct };
            }
            catch (Exception ex)
            {
                return new ResponseApi<ProductModel>() { Status = 500, Message = ex.Message };
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(ResponseListApi<ProductModel>), 200)]
        [ProducesResponseType(typeof(ResponseListApi<ProductModel>), 500)]
        public async Task<ResponseListApi<ProductModel>> GerAllProduct()
        {
            try
            {
                List<ProductModel> products = await _productService.GetAllProductsAsync();
                return new ResponseListApi<ProductModel>() { Status = 200, Data = products };
            }
            catch (Exception ex)
            {
                return new ResponseListApi<ProductModel>() { Status = 500, Message = ex.Message };
            }
        }
        
        [HttpGet("{uuid}")]
        [ProducesResponseType(typeof(ResponseApi<ProductModel>), 200)]
        [ProducesResponseType(typeof(ResponseApi<ProductModel>), 500)]
        public async Task<ResponseApi<ProductModel>> GetProduct([FromRoute] string uuid)
        {
            try
            {
                ProductModel product = await _productService.GetProductById(uuid);
                return new ResponseApi<ProductModel>() { Status = 200, Data = product };
            }
            catch (Exception ex)
            {
                return new ResponseApi<ProductModel>() { Status = 500, Message = ex.Message };
            }
        }

        [HttpPatch("{uuid}")]
        [ProducesResponseType(typeof(ResponseApi), 200)]
        [ProducesResponseType(typeof(ResponseApi), 500)]
        public async Task<ResponseApi> UpdateProduct([FromRoute] string uuid, [FromBody] ProductUpdateDTO requestDto)
        {
            ProductUpdateDTOValidator validator = new ();
            
            // Execute the validator
            var results = validator.Validate(requestDto);

            if (!results.IsValid)
            {
                string errorValidation = string.Join(Environment.NewLine, results.Errors.Select(e => e.ErrorMessage));
                return new  ResponseApi() { Status = 400 , Message = errorValidation};
            }

            try
            {
                string responseMessgae = $"No records were found with the ID {uuid} to update.";
                bool delProduct = await _productService.UpdateProductAsync(uuid,requestDto);
                if(delProduct) responseMessgae = $"The product with ID {uuid} has been update.";
                return new ResponseApi() { Status = 200, Message = responseMessgae };
            }
            catch (Exception ex)
            {
                return new ResponseApi() { Status = 500, Message = $"An error occurred while trying to update the product with ID {uuid}. Details: {ex.Message}"};
            }
        }

        [HttpDelete("{uuid}")]
        [ProducesResponseType(typeof(ResponseApi), 200)]
        [ProducesResponseType(typeof(ResponseApi), 500)]
        public async Task<ResponseApi> DeleteProduct([FromRoute] string uuid)
        {
            try
            {
                string responseMessgae = $"No records were found with the ID {uuid} to delete.";
                bool delProduct = await _productService.DeleteProductAsync(uuid);
                if(delProduct) responseMessgae = $"The product with ID {uuid} has been deleted.";
                return new ResponseApi() { Status = 200, Message = responseMessgae };
            }
            catch (Exception ex)
            {
                return new ResponseApi() { Status = 500, Message = $"An error occurred while trying to delete the product with ID {uuid}. Details: {ex.Message}"};
            }
        }
    }
}