using Microsoft.AspNetCore.Mvc;
using scaffolding_net_7.src.interfaces.IServices;
using scaffolding_net_7.src.dto;
using scaffolding_net_7.src.interfaces.IControllers;

namespace scaffolding_net_7.src.controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HealthController : ControllerBase, IHealthController
    {
        private readonly IHealthService _healthService;

        public HealthController(IHealthService healthService)
        {
            _healthService = healthService;
        }

        [HttpGet]
        public HealthResponse Get()
        {
            return _healthService.GetHealth();
        }
    }
}