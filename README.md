# Scaffolding NET 7
## Project Structure for Domain-Driven Design (DDD) in .NET 7

This project follows a structure inspired by Domain-Driven Design (DDD), a software design methodology that focuses on understanding and modeling the application's domain. Below are descriptions of key folders in this project:

### controllers: 
This folder contains controllers in the presentation layer, which handle user interactions and send commands to the application layer.

### data: 
Here, implementations of repositories and infrastructure services related to data access can be found. The infrastructure layer handles persistence and data access.

### dto: 
Data Transfer Objects (DTOs) are used to transfer data between layers and to represent specific views of domain objects.

### interfaces: 
This folder might contain interfaces for application services, repositories, and other components.

### models: 
Contains entities and value objects representing key domain concepts. These models reflect the domain structure and may have associated behaviors.

### services: 
In DDD, this folder would contain application services responsible for coordinating operations and executing application logic. These services orchestrate interaction between domain objects.

It's important to note that the application of DDD principles can vary based on the context and specific project requirements. The structure presented here is an adaptation that can be modified according to the project's needs.

This .NET / project follows an architecture that encourages a clear and modular design, facilitating code maintenance and evolution as the application develops.

## Description
This project serves as a quick starting point for building applications using a clear architecture in .NET. It contains two domains:

1. **Health**: This domain provides a simple endpoint to check if the service is up and running correctly.
2. **Products CRUD**: The project includes a CRUD (Create, Read, Update, Delete) functionality for managing products. It demonstrates a connection to MongoDB and can be used as a reference for building similar features in your applications.

## Requirements to Run the Project

- [.NET 7](https://dotnet.microsoft.com/download/dotnet/7.0)
- [Docker](https://www.docker.com/) (for running MongoDB and MinIO containers or have them installed previously)

For Docker images related to this project, you can find them in [this repository](https://gitlab.com/atalivar77/docker-containers-templates).

## Installation
To get started with this project, all you need to do is:

1. Clone this repository to your local machine.
2. Run `dotnet watch` in the terminal.

These two simple steps will have you up and running with the project. No additional dependencies or complex installation processes are required. Enjoy!

## Screenshots:

### Swagger endpoints
![Imagen](https://gitlab.com/atalivar77/scaffolding-net-7/-/raw/main/images/01.png?ref_type=heads)

### Swagger Schemas
![Imagen](https://gitlab.com/atalivar77/scaffolding-net-7/-/raw/main/images/02.png?ref_type=heads)
